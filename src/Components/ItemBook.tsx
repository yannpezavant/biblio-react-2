import { IBook } from "@/entities";

interface Props{
    book:IBook;
}

export default function ItemBook({book}:Props){
    return(
        <>
        <h3>{book.title}</h3>
        <p>{book.author}</p>
        <p>{book.disponibility}</p>
        </>
    )
}