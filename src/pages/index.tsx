import { fetchAllBooks } from "@/book-service";
import ItemBook from "@/Components/ItemBook";
import { IBook } from "@/entities";
import { GetServerSideProps } from "next";
import React from "react";
import { Col, Row } from "react-bootstrap";

interface Props{
  books:IBook[];
}
export default function Index({books}:Props){
  return(
    <Row gutter={[10, 10]} justify="center">
        {books.map(item =>
          <Col xs={12} md={6} key={item.id} >
            <ItemBook book={item} />
          </Col>
        )}
      </Row>

  )
}

export const GetServerSideProps: GetServerSideProps<Props> = async () => {

  return {
    props: {
      books: await fetchAllBooks() //fait un fetch du serveur Symfony

    }
  }
}
