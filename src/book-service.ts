import axios from "axios";
import { IBook } from "./entities";

export async function fetchAllBooks() {
    const response = await axios.get<IBook[]>('/api/book');
    return response.data;
}
